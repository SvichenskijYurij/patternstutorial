﻿using System;
using System.Numerics;

namespace PatternsTutorial.CalcLogic
{
    public class ComplexCalculator : ICalculator<Complex, string>
    {
        internal ComplexCalculator()
        {
            Memory = 0;
        }

        public Complex Memory { get; set; }

        public Complex Plus(Complex first, Complex second)
        {
            return  first + second;
        }

        public Complex Minus(Complex first, Complex second)
        {
            return first - second;
        }

        public Complex Multiply(Complex first, Complex second)
        {
            return first * second;
        }

        public Complex Divide(Complex first, Complex second)
        {
            if(second == 0)
                throw new DivideByZeroException();
            return first / second;
        }

        public Complex Calculate(Complex first, Complex second, string operation)
        {
            switch (operation)
            {
                case "+":
                    return Plus(first, second);
                case "-":
                    return Minus(first, second);
                case "*":
                    return Multiply(first, second);
                case "/":
                    return Divide(first, second);
                default:
                    throw new Exception("No such operation!");
            }
        }
    }
}
