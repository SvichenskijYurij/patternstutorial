﻿namespace PatternsTutorial.CalcLogic
{
    public class CalculatorFactory
    {
        public static object CreateCalculator(CalculatorTypeEnum type)
        {
            switch (type)
            {
                case CalculatorTypeEnum.Digit:
                    return new DigitCalculator();
                case CalculatorTypeEnum.Complex:
                    return new ComplexCalculator();
                default:
                    return null;
            }
        }
    }
}
