﻿using System;
using PatternsTutorial.CalcLogic;

namespace PatternsTutorial.ConsoleCalculator
{
    public class CalculatorManager
    {

        public CalculatorManager()
        {
            ShowMenu();
        }

        private void ShowMenu()
        {
            Console.WriteLine("Choose your calculator:");
            Console.WriteLine("1) Digit calculator\n2) Complex calculator\n3) Exit");

            try
            {
                var calcType = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (calcType == 1)
                {
                    new DigitConsoleCalculator((DigitCalculator)CalculatorFactory.CreateCalculator(CalculatorTypeEnum.Digit));
                }
                if (calcType == 2)
                {
                    new ComplexConsoleCalculator((ComplexCalculator)CalculatorFactory.CreateCalculator(CalculatorTypeEnum.Complex));
                }
                if (calcType == 3)
                {
                    Environment.Exit(0);
                }
            }
            catch (Exception)
            {
                Console.Clear();
                ShowMenu();
            }
        }
    }
}
