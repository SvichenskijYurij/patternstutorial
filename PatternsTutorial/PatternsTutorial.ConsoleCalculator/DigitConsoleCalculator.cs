﻿using System;
using PatternsTutorial.CalcLogic;

namespace PatternsTutorial.ConsoleCalculator
{
    public class DigitConsoleCalculator
    {
        private readonly DigitCalculator _calculator;

        public DigitConsoleCalculator(DigitCalculator calculator)
        {
            _calculator = calculator;
            ShowCalculator();
        }

        private void ShowCalculator()
        {
            Console.WriteLine(_calculator.Memory);
            string operation = Console.ReadLine();

            if (operation.Equals("exit"))
            {
                Environment.Exit(0);
            }

            try
            {
                double number = Convert.ToDouble(Console.ReadLine());
                _calculator.Memory = _calculator.Calculate(_calculator.Memory, number, operation);
                Console.Clear();
            }
            catch (Exception exception)
            {
                Console.Clear();
                Console.WriteLine(exception.Message);
            }
            finally
            {
                ShowCalculator();
            }
        }

    }
}
