﻿using System;
using System.Numerics;
using PatternsTutorial.CalcLogic;

namespace PatternsTutorial.ConsoleCalculator
{
    public class ComplexConsoleCalculator
    {
        private readonly ComplexCalculator _calculator;

        public ComplexConsoleCalculator(ComplexCalculator complexCalculator)
        {
            _calculator = complexCalculator;
            ShowCalculator();
        }

        private void ShowCalculator()
        {
            Console.WriteLine(_calculator.Memory);
            string operation = Console.ReadLine();

            if (operation.Equals("exit"))
            {
                Environment.Exit(0);
            }

            try
            {
                var real = Convert.ToDouble(Console.ReadLine());
                var imaginary = Convert.ToDouble(Console.ReadLine());
                var complex = new Complex(real, imaginary);

                _calculator.Memory = _calculator.Calculate(_calculator.Memory, complex, operation);

                Console.Clear();
            }
            catch (Exception exception)
            {
                Console.Clear();
                Console.WriteLine(exception.Message);
            }
            finally
            {
                ShowCalculator();
            }
        }
    }
}
