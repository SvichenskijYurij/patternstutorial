﻿namespace Calculator.CalculatorLogic
{
    interface ICalculator<TArgument>
    {
        TArgument Plus(TArgument first, TArgument second);
        TArgument Minus(TArgument first, TArgument second);
        TArgument Multiply(TArgument first, TArgument second);
        TArgument Divide(TArgument first, TArgument second);
    }
}
