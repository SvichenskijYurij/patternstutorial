﻿using System;
using System.Numerics;

namespace Calculator.CalculatorLogic
{
    public class ComplexCalculator : Calculator, ICalculator<Complex>
    {
        internal ComplexCalculator()
        {
            MemoryNumber = "0+0i";
        }
        public sealed override string MemoryNumber { get; set; }
        public Complex Plus(Complex first, Complex second)
        {
            return first + second;
        }
        public Complex Minus(Complex first, Complex second)
        {
            return first - second;
        }
        public Complex Multiply(Complex first, Complex second)
        {
            return first * second;
        }
        public Complex Divide(Complex first, Complex second)
        {
            if (second == 0)
                throw new DivideByZeroException();
            return first / second;
        }
        public override string Calculate(string first, string second, string operation)
        {
            Complex firstArg = Converter.ToComplex(first);
            Complex secondArg = Converter.ToComplex(second);

            switch (operation)
            {
                case "+":
                    return Plus(firstArg, secondArg).ToSting();
                case "-":
                    return Minus(firstArg, secondArg).ToSting();
                case "*":
                    return Multiply(firstArg, secondArg).ToSting();
                case "/":
                    return Divide(firstArg, secondArg).ToSting();
                default:
                    throw new Exception("No such operation!");
            }
        }
    }
}
