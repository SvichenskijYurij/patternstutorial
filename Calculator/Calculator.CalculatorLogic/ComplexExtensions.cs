﻿using System.Numerics;

namespace Calculator.CalculatorLogic
{
    public static class ComplexExtensions
    {
        public static string ToSting(this Complex comlex)
        {
            if (comlex.Imaginary >= 0)
            {
                return $"{comlex.Real}+{comlex.Imaginary}i";
            }
            else
            {
                return $"{comlex.Real}{comlex.Imaginary}i";
            }
        }
    }
}
