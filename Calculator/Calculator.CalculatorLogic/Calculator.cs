﻿namespace Calculator.CalculatorLogic
{
    public abstract class Calculator
    {
        public abstract string MemoryNumber { get; set; }
        public abstract string Calculate(string first, string second, string operation);
    }
}
