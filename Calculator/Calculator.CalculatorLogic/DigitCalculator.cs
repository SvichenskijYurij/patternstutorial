﻿using System;
using System.Globalization;

namespace Calculator.CalculatorLogic
{
    public class DigitCalculator : Calculator, ICalculator<double>
    {
        internal DigitCalculator()
        {
            MemoryNumber = "0";
        }
        public sealed override string MemoryNumber { get; set; }
        public double Plus(double first, double second)
        {
            return first + second;
        }
        public double Minus(double first, double second)
        {
            return first - second;
        }
        public double Multiply(double first, double second)
        {
            return first * second;
        }
        public double Divide(double first, double second)
        {
            if (second.Equals(0))
                throw new DivideByZeroException();
            return first / second;
        }

        public override string Calculate(string first, string second, string operation)
        {
            double firstArg = Convert.ToDouble(first);
            double secondArg = Convert.ToDouble(second);
            switch (operation)
            {
                case "+":
                    return Plus(firstArg, secondArg).ToString(CultureInfo.InvariantCulture);
                case "-":
                    return Minus(firstArg, secondArg).ToString(CultureInfo.InvariantCulture);
                case "*":
                    return Multiply(firstArg, secondArg).ToString(CultureInfo.InvariantCulture);
                case "/":
                    return Divide(firstArg, secondArg).ToString(CultureInfo.InvariantCulture);
                default:
                    throw new Exception("No such operation!");
            }
        }
    }
}
