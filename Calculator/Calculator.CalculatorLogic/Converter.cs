﻿using System.Globalization;
using System.Numerics;
using System.Text.RegularExpressions;

namespace Calculator.CalculatorLogic
{
    public static class Converter
    {
        public static Complex ToComplex(string arg)
        {
            // The pattern has been broken down for educational purposes
            string regexPattern =
                // Match any float, negative or positive, group it
                @"([-+]?\d+\.?\d*|[-+]?\d*\.?\d+)" +
                // ... possibly following that with whitespace
                @"\s*" +
                // ... followed by a plus
                @"\+" +
                // and possibly more whitespace:
                @"\s*" +
                // Match any other float, and save it
                @"([-+]?\d+\.?\d*|[-+]?\d*\.?\d+)" +
                // ... followed by 'i'
                @"i";

            Regex regex = new Regex(regexPattern);
            Match match = regex.Match(arg);
            double real = double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture);
            double imaginary = double.Parse(match.Groups[2].Value, CultureInfo.InvariantCulture);

            return new Complex(real, imaginary);
        }

        public static string ToString(Complex arg)
        {
            if (arg.Imaginary > 0)
            {
                return $"{arg.Real}+{arg.Imaginary}";
            }
            else
            {
                return $"{arg.Real}{arg.Imaginary}";
            }
        }
    }
}
